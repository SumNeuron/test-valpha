import Vuex from 'vuex'

import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';
import state from './state.js';


export const strict = false

const modules = {
}

export {
  mutations,
  actions,
  getters,
  state,
  modules
}
