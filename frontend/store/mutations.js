import Vue from 'vue'

const mutations = {
  set(state, {k, v}) { Vue.set(state, k, v) },
  join(state, {k, v}) { Vue.set(state, k, Object.assign({}, state[k], v)) },
  push(state, {k, v}) { state[k].push(v) },
  sset(state, {k, v, s}) { Vue.set(state[k], s, v) },
  splice(state, {k, i}) { state[k].splice(i, 1) },
}


export default mutations
