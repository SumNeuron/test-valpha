# Test-valpha
This repository is to explicitly to test the component `VRecordsTable` from
[`valpha`](https://www.npmjs.com/package/valpha)

Useful links:

- [npm](https://www.npmjs.com/package/valpha)
- [git](https://www.npmjs.com/package/valpha)
- [rollup.config.js](https://gitlab.com/SumNeuron/valpha/blob/master/rollup.config.js)
- [package.json](https://gitlab.com/SumNeuron/valpha/blob/master/package.json)
- [nuxt.config.js](https://gitlab.com/SumNeuron/valpha/blob/master/nuxt.config.js)
- [entry.js](https://gitlab.com/SumNeuron/valpha/blob/master/entry.js)
- [`VRecordsTable`](https://gitlab.com/SumNeuron/valpha/blob/master/components/VRecordsTable.vue)


Useful snippets:

```bash
docker-compose -f docker-compose.development.yml build
docker-compose -f docker-compose.development.yml up
docker-compose -f docker-compose.development.yml down
```
